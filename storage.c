/*
ASPDB

File: storage.c 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <string.h>



#include "h/data.h"
#include "h/common.h"
#include "h/aspdb.h"

/*
#ifndef DIRTYBUILD
#define DIRTYBUILD
#endif

#ifdef DIRTYBUILD
#include "../blockarchive/barch.h"
#else
#include "barch.h"
#endif*/

/* Could I use a macro here instead? Yes, but are painful to write*/
/* It returns the size of superbloc in bytes */
uint16_t calculate_dynamic_superbloc_size(int size_blk)
{
    uint16_t tmp, mod;

    tmp=((sizeof(sHeadPdba_ele_t))*size_blk);
    mod= tmp%(sizeof(sPassData_t));
    return (mod ==0) ? tmp : (tmp+ ((sizeof(sPassData_t))- mod));

}


void first_time_init_sb(sBarch_desc_t* bd, sHeadP_t *head)
{
    for(int i=0;i<(int)bd->hdesc->bn;i++)
    {
        head->hdbe[i].blockid=i;
        memset(head->hdbe[i].label, 0x00, LABELSIZE);
        if(i<bd->hdesc->spblk)
            head->hdbe[i].flg=(ENTRYUSED|ENTRYSP);
        else
            head->hdbe[i].flg=ENTRYEMPTY;
    }
}


/* Size is referred in blocks */
void alloc_sb(sHeadP_t *head, uint32_t size_blk)
{
    uint16_t sb_szbyte;

    sb_szbyte=calculate_dynamic_superbloc_size(size_blk);
    head->hdbe=malloc((size_t)sb_szbyte);
    head->init=1;
}

int update_header_sp(sBarch_desc_t *bdesc, sHeadP_t *head)
{
    return barch_write_sblock(bdesc, (void*)head->hdbe);
}


int create_passwdba_custom (char *name, char *generator, uint32_t capacity)
{
    int status=STATUS_OK;
    sHeadP_t head;
    sBarch_desc_t bd;
    aspdb_dbg_print_create_archive_custom(name, generator, capacity);

    status=barch_create_archive(name, generator, sizeof(sPassData_t), (uint32_t)(capacity), 
        (uint16_t)calculate_dynamic_superbloc_size(capacity));

    open_passwdba(&bd, name, &head);
    alloc_sb(&head, capacity);
    first_time_init_sb(&bd, &head);
    update_header_sp(&bd, &head);
    close_passwdba(&bd, &head);  

    return status;  
}

int open_passwdba(sBarch_desc_t *bdesc, char *name, sHeadP_t *head)
{
    int status=0;

    barch_open_archive(bdesc, name, BTAB_STATUS_SPEC|BTAB_STATUS_PROCT);
    alloc_sb(head, bdesc->hdesc->bn);

    status=barch_read_sblock(bdesc, (void*)head->hdbe);

    return status;
}


int close_passwdba(sBarch_desc_t *bdesc, sHeadP_t *head)
{
    update_header_sp(bdesc, head);
    free(head->hdbe);
    head->init=0;

    return barch_close_archive(bdesc);

}



int write_new_entry(sPassData_t *pass, sBarch_desc_t *bdesc, sHeadP_t *head)
{
    int status=0;
    uint32_t id=0;

    status=barch_write_current_block(bdesc, pass);
    PWDM_DEBUG("Trying to add entry %s in blockid %d exited with status %d\n", pass->label,  bdesc->curr, status );

    if(status == STATUS_OK)
    {
        id=bdesc->curr;
        head->hdbe[id].blockid=id;
        strncpy((char*)head->hdbe[id].label, (char*)pass->label, LABELSIZE);
        head->hdbe[id].flg=ENTRYUSED;

        status=update_header_sp(bdesc, head);
    }
    return status;
}


int read_entry(sPassData_t *pass, sBarch_desc_t *bdesc, uint32_t id)
{
    return barch_read_block(bdesc, (void*)pass,id);
}

int delete_entry(sBarch_desc_t *bdesc, sHeadP_t *head,  uint32_t id)
{
    int status=STATUS_OK;

    status=barch_delete_block(bdesc, id);
    if(status == STATUS_OK)
    {
        head->hdbe[id].blockid=0;
        memset(head->hdbe[id].label, 0x00, LABELSIZE);
        head->hdbe[id].flg=ENTRYEMPTY;
    }

    return status;
}