# Makefile for ASPDB: A.nother (or A.lexjan in the rare moments of self-esteem)
# S.mall P.assword D.atabase

# Alexjan Carraturo

# Alexjan Carraturo

CC=$(CROSS_COMPILE)gcc
AR=$(CROSS_COMPILE)ar




#Keep in mind that libasba header and library are needed. 
#If not in the library path, have to be added somehow. 
# DIRTYBUILD is an option that tell the source code to search 
# headers of libasba in local path. 

DBGOPT=-D DBG -ggdb #-D DIRTYBUILD
CFLAGS := -Wall -I.  -O0 -g $(DBGOPT)  #-I ../blockarchive/ 
LDFLAGS := -lssl -lcrypto -lasba -L. #-L ../blockarchive/

LIB=aspdb
LIB_SRC := aspdb_cli.c minput.c  crypt1.c search.c storage.c debug.c 
LIBFLAGS := $(LDFLAGS) -fPIC -shared -Wl,-soname,lib$(LIB).so
LIB_OBJ := aspdb.o

TLDFLAGS := $(LDFLAGS) -laspdb
TCFLAGS  := $(CFLAGS)
SRC := aspdb_cmd.c
OBJS := aspdb_cmd.o
CLICMD := aspdb_cmd 


#EXTRA_CFLAGS := -Wall -I. -lssl -lcrypto -lasba $(DBGOPT)
#EXTRA_CFLAGS := -Wall -I. -lssl -lcrypto -lasba $(DBGOPT)


all : $(LIB) $(CLICMD)

$(LIB) : $(LIB_SRC)
	$(CC) $(CFLAGS) $(LIBFLAGS) $^ -o lib$@.so
	ln -s lib$(LIB).so lib$(LIB).so.1

$(OBJS) : $(SRC)
	$(CC) $(TCFLAGS) -c $^ -o $@
$(CLICMD) : $(OBJS) 
	$(CC) $(TLDFLAGS) $(TCFLAGS) $^ -o $@ 

clean: 
	rm -rfv *.o
	rm -rfv *.a
	rm -rfv lib$(LIB).*
	rm -rfv $(CLICMD)