/*
ASPDB

File: crypt1.c 
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/


/**
  Programmer happiness-optimized AES256 encryption/decryption
  using EVP APIs.
  Based on public domain code by
  Saju Pillai (saju.pillai@gmail.com)
**/

#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define AES_BLOCK_SIZE 32

static unsigned int salt[] = {12345, 54321}; /* Random salt, not exported */

/**
 * Create a 256 bit key and IV using the supplied key_data. salt can be added
 *for taste. Fills in the encryption and decryption ctx objects and returns 0 on
 *success
 **/
int aes_init(unsigned char *key_data, int key_data_len, unsigned char *salt,
             EVP_CIPHER_CTX *e_ctx, EVP_CIPHER_CTX *d_ctx) {
  int i, nrounds = 5;
  unsigned char key[32], iv[32];

  /*
   * Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the
   * supplied key material. nrounds is the number of times the we hash the
   * material. More rounds are more secure but slower.
   */
  i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt, key_data,
                     key_data_len, nrounds, key, iv);
  if (i != 32) {
    printf("Key size is %d bits - should be 256 bits\n", i);
    return -1;
  }

  EVP_CIPHER_CTX_init(e_ctx);
  EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, key, iv);
  EVP_CIPHER_CTX_init(d_ctx);
  EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv);

  return 0;
}

/*
 * Encrypt *len bytes of data
 * All data going in & out is considered binary (unsigned char[])
 */
unsigned char *aes_encrypt(EVP_CIPHER_CTX *e, unsigned char *plaintext,
                           int *len) {
  /* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1
   * bytes */
  int c_len = *len + AES_BLOCK_SIZE, f_len = 0;
  unsigned char *ciphertext = malloc(c_len);
  memset(ciphertext, 0x00, c_len);

  /* allows reusing of 'e' for multiple encryption cycles */
  EVP_EncryptInit_ex(e, NULL, NULL, NULL, NULL);

  /* update ciphertext, c_len is filled with the length of ciphertext generated,
   *len is the size of plaintext in bytes */
  EVP_EncryptUpdate(e, ciphertext, &c_len, plaintext, *len);

  /* update ciphertext with the final remaining bytes */
  EVP_EncryptFinal_ex(e, ciphertext + c_len, &f_len);

  *len = c_len + f_len;
  return ciphertext;
}

/*
 * Decrypt *len bytes of ciphertext
 */
unsigned char *aes_decrypt(EVP_CIPHER_CTX *e, unsigned char *ciphertext,
                           int *len) {
  /* plaintext will always be equal to or lesser than length of ciphertext*/
  int p_len = *len, f_len = 0;
  unsigned char *plaintext = malloc(p_len);

  EVP_DecryptInit_ex(e, NULL, NULL, NULL, NULL);
  EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, *len);
  EVP_DecryptFinal_ex(e, plaintext + p_len, &f_len);

  *len = p_len + f_len;
  return plaintext;
}


void *encrypt1(unsigned char *enc_pass, unsigned char *cleartext, char *key)
{
    EVP_CIPHER_CTX *en, *de;
    unsigned char *key_data;
    int key_data_len;
    int len;
    unsigned char *ciphertext;

    en = EVP_CIPHER_CTX_new();
    de = EVP_CIPHER_CTX_new();
    
    key_data = (unsigned char *)key;
    key_data_len = strlen(key);

    if (aes_init(key_data, key_data_len, (unsigned char *)&salt, en, de)) 
    {
        printf("Couldn't initialize AES cipher\n");
    }
    else
    {
        len = strlen((char*)cleartext) + 1;
        ciphertext =aes_encrypt(en, (unsigned char *)cleartext, &len);
        strncpy((char*)enc_pass, (char*)ciphertext, strlen((char*)ciphertext));
        free(ciphertext);

        EVP_CIPHER_CTX_cleanup(en);
        EVP_CIPHER_CTX_cleanup(de);
        EVP_CIPHER_CTX_free(en);
        EVP_CIPHER_CTX_free(de);
    }
    return NULL;
}


void *decrypt1(unsigned char *dec_pass, unsigned char *ciphertext, char *key)
{
    EVP_CIPHER_CTX *en, *de;
    unsigned char *key_data;
    int key_data_len;
    int len;
    char *cleartext;

    en = EVP_CIPHER_CTX_new();
    de = EVP_CIPHER_CTX_new();
    key_data = (unsigned char *)key;
    key_data_len = strlen(key);

    if (aes_init(key_data, key_data_len, (unsigned char *)&salt, en, de)) 
    {
        printf("Couldn't initialize AES cipher\n");
    }
    else
    {
        len = strlen((char*)ciphertext) + 1;
        cleartext =(char *) aes_decrypt(de, (unsigned char *)ciphertext, &len);
        strncpy((char*)dec_pass, (char*)cleartext, strlen(cleartext));
        free(cleartext);

        EVP_CIPHER_CTX_cleanup(en);
        EVP_CIPHER_CTX_cleanup(de);
        EVP_CIPHER_CTX_free(en);
        EVP_CIPHER_CTX_free(de);
    }
    return NULL;

}




/*
char *encrypt(char *cleartext, char *key) {
  EVP_CIPHER_CTX *en, *de;
  en = EVP_CIPHER_CTX_new();
  de = EVP_CIPHER_CTX_new();

  unsigned char *key_data = (unsigned char *)key;
  int key_data_len = strlen(key);

  if (aes_init(key_data, key_data_len, (unsigned char *)&salt, en, de)) {
    printf("Couldn't initialize AES cipher\n");
    return 0;
  }

  int len = strlen(cleartext) + 1;
  unsigned char *ciphertext = aes_encrypt(en, (unsigned char *)cleartext, &len);

  EVP_CIPHER_CTX_cleanup(en);
  EVP_CIPHER_CTX_cleanup(de);
  EVP_CIPHER_CTX_free(en);
  EVP_CIPHER_CTX_free(de);

  return (char *)ciphertext;
}*/


/*
char *decrypt(char *ciphertext, char *key) {
  EVP_CIPHER_CTX *en, *de;
  en = EVP_CIPHER_CTX_new();
  de = EVP_CIPHER_CTX_new();

  unsigned char *key_data = (unsigned char *)key;
  int key_data_len = strlen(key);

  if (aes_init(key_data, key_data_len, (unsigned char *)&salt, en, de)) {
    printf("Couldn't initialize AES cipher\n");
    return 0;
  }

  int len = strlen(ciphertext) + 1;
  unsigned char *cleartext = aes_decrypt(de, (unsigned char *)ciphertext, &len);

  EVP_CIPHER_CTX_cleanup(en);
  EVP_CIPHER_CTX_cleanup(de);
  EVP_CIPHER_CTX_free(en);
  EVP_CIPHER_CTX_free(de);

  return (char *)cleartext;
}
*/