/*
ASPDB

File: search.c
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "h/common.h"
#include "h/data.h"
#include "h/aspdb.h"



/* This assumes that the pattern searched is <= of actual label */

int search_text(char *pattern, char *label )
{
    int i=0,elen=0,slen=0;
    char *cmp=NULL;

    slen=strlen(pattern);
    elen=strlen(label);

    cmp=label;
    while(i<=(elen-slen))
    {
        if( strncmp(pattern, cmp, slen) == 0 )
        {
            return 1;
        }
        else
        {
            cmp++;
            i++;
        }
    }
    return 0;

}

int search_entry_table_num(char *pattern, sHeadP_t *head)
{
    int tot=0;
    for(int i=0; i<DBCAPTY;i++)
    {
        if( head->hdbe[i].flg == ENTRYUSED)
        {
            if (search_text(pattern, (char*)head->hdbe[i].label) )
            {
                tot++;
            }
        }
    }
    return tot;
}


int search_entry_table(char *pattern, sHeadP_t *head, sSrcRes_t *res)
{
    int j=0;
    res->size=search_entry_table_num(pattern, head);
    if(res->size > 0)
    {
        res->indexes=malloc(res->size*sizeof(uint32_t));
        for(int i=0; i<DBCAPTY;i++)
        {
            if( head->hdbe[i].flg == ENTRYUSED)
            {
                if (search_text(pattern, (char*)head->hdbe[i].label) )
                {
                    res->indexes[j]=i;
                    j++;
                }
            }
        }
        //indexes to be freed somewhere
        return STATUS_OK;
    }
    else
        return NOENTRY;

}